# Copyright (C) 2018 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the plinth package.
#
# Rui Branco - DebianPT <ruipb@debianpt.org>, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: plinth 0.37.0\n"
"Report-Msgid-Bugs-To: plinth@packages.debian.org\n"
"POT-Creation-Date: 2018-07-03 16:39+0530\n"
"PO-Revision-Date: 2018-09-27 15:33+0100\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Last-Translator: Rui Branco - DebianPT <ruipb@debianpt.org>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"

#. Type: note
#. Description
#: ../templates:1001
msgid "FreedomBox first wizard secret - ${secret}"
msgstr "Primeiro segredo FreedomBox - ${secret}"

#. Type: note
#. Description
#: ../templates:1001
msgid ""
"Please save this string. You will be asked to enter this in the first screen "
"after you launch the FreedomBox interface. In case you lose it, you can find "
"it in the file /var/lib/plinth/firstboot-wizard-secret."
msgstr ""
"Por favor guarde esta 'string'. Ser-lhe-á pedido para a introduzir na "
"primeira tela assim que lance a interface FreedomBox. No caso de a perder, "
"poderá encontrá-la no ficheiro /var/lib/plinth/firstboot-wizard-secret."
